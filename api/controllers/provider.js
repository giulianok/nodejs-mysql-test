const Service = require('../services/provider.service');

module.exports = {

    getAll: function (req, res) {

        Service.findAll()
            .then((response) => {
                res.json(response);
            })
            .catch((err) => {
                res.json(err);
            });

    },

    create: function (req, res) {
        const data = req.body;

        console.log(data);

        Service.create(data)
            .then((response) => {
                res.json(response);
            })
            .catch((err) => {
                res.json(err);
            })
        ;
    },

    findById: function (req, res) {
        let id = req.swagger.params.id.value;

        Service.findById(id)
            .then((data) => {
                res.json(data);
            })
            .catch((err) => {
                res.json({
                    message: err
                });
            });
    },

    update: function (req, res) {
        const id = req.swagger.params.id.value;
        const data = req.body;

        Service.update(id, data)
            .then((data) => {
                res.json({
                    message: data
                });
            })
            .catch((err) => {
                res.json({
                    message: err
                });
            });
    },

    delete: function (req, res) {
        const id = req.swagger.params.id.value;

        Service.delete(id)
            .then((data) => {
                res.json({
                    message: data
                });
            })
            .catch((err) => {
                console.log(err);
                res.json({
                    message: err
                });
            });
    }

};