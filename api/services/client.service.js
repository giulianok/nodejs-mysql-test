const Model = require('../models/client.model');
const Provider = require('../models/provider.model');
const ProviderService = require('../services/provider.service');

const ClientService = {

    findAll: function () {
        return new Promise((resolve, reject) => {

            Model.findAll({
                include: [{
                    model: Provider
                }]
            })
                .then((data) => {
                    resolve(data);
                })
                .catch((err) => {
                    console.error(err);
                    reject({
                        message: `There was an error. Please try again`
                    });
                })
            ;

        });
    },

    create: function (data) {
        return new Promise((resolve, reject) => {

            Model.create(data)
                .then((client) => {
                    const message = 'The client has been created';
                    if (client) {
                        if (data.providers) {
                            ProviderService.findAllByIDs(data.providers)
                                .then((providers) => {
                                    client.setProviders(providers);

                                    return resolve({
                                        message: message
                                    });
                                })
                        } else {
                            return resolve({
                                message: message
                            });
                        }
                    }
                    reject({
                        message: 'The client has not been created'
                    });
                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                })
            ;

        });
    },

    findById: function (id) {
        return new Promise((resolve, reject) => {
            Model.findOne({
                where: {
                    id: id
                }
            })
                .then((client) => {
                    if (!client) {
                        return reject(`Client not found`);
                    }
                    resolve(client.dataValues);
                })
        });
    },

    update: function (id, data) {
        return new Promise((resolve, reject) => {

            if (data.id) {
                throw new Error(`The ID cannot be inside of Data`);
            }

            Model.update(data, {
                where: {
                    id: id
                }
            })
                .then(() => {

                    Model.findById(id)
                        .then((client) => {
                            const message = 'The client has been created';

                            if (client) {
                                ProviderService.findAllByIDs(data.providers)
                                    .then((providers) => {
                                        client.setProviders(providers || null);

                                        return resolve({
                                            message: message
                                        });
                                    })
                            }

                            reject({
                                message: 'The client has not been created'
                            });
                        })
                        .catch((err) => {
                            let message = `Server error. Try again`;

                            if (typeof err === 'string') {
                                message = err;
                            } else {
                                console.log(err);
                            }

                            reject({
                                message: message
                            });
                        });

                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                });

        });
    },

    delete: function (id) {
        return new Promise((resolve, reject) => {

            if (!id) {
                throw new Error(`The ID is required`);
            }

            Model.destroy({
                where: {
                    id: id
                }
            })
                .then((result) => {
                    resolve(`The client has been removed`);
                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                });

        });
    }

};


module.exports = ClientService;