const Model = require('../models/provider.model');

const ProviderService = {

    findAll: function () {
        return new Promise((resolve, reject) => {

            Model.findAll()
                .then((data) => {
                    resolve(data);
                })
                .catch((err) => {
                    console.error(err);
                    reject({
                        message: `There was an error. Please try again`
                    });
                })
            ;

        });
    },

    findAllByIDs: function (ids) {
        return new Promise((resolve, reject) => {

            Model.findAll({
                where: {
                    id: ids
                }
            })
                .then((data) => {
                    resolve(data);
                })
                .catch((err) => {
                    console.error(err);
                    reject({
                        message: `There was an error. Please try again`
                    });
                })
            ;

        });
    },

    create: function (data) {
        return new Promise((resolve, reject) => {

            Model.create(data)
                .then(() => {
                    resolve({
                        message: 'The provider has been created'
                    });
                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                })
            ;

        });
    },

    findById: function (id) {
        return new Promise((resolve, reject) => {
            Model.findOne({
                where: {
                    id: id
                }
            })
                .then((client) => {
                    if (!client) {
                        return reject(`Provider not found`);
                    }
                    resolve(client.dataValues);
                })
        });
    },

    update: function (id, data) {
        return new Promise((resolve, reject) => {

            if (data.id) {
                throw new Error(`The ID cannot be inside of Data`);
            }

            Model.update(data, {
                where: {
                    id: id
                }
            })
                .then((result) => {
                    resolve(`The provider ${result.name} has been updated`);
                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                });

        });
    },

    delete: function (id) {
        return new Promise((resolve, reject) => {

            if (!id) {
                throw new Error(`The ID is required`);
            }

            Model.destroy({
                where: {
                    id: id
                }
            })
                .then((result) => {
                    resolve(`The provider has been removed`);
                })
                .catch((err) => {
                    let message = `Server error. Try again`;

                    if (typeof err === 'string') {
                        message = err;
                    } else {
                        console.log(err);
                    }

                    reject({
                        message: message
                    });
                });

        });
    }

};


module.exports = ProviderService;