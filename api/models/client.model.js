const Sequelize = require('sequelize');
const database = require(__base + 'database');
const validator = require('validator');

const Client = database.define('client', {

    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: { // This property doesn't work and I don't know. I implemented a custom validation
            notEmpty: true
        }
    },
    email: {
        type: Sequelize.STRING,
        validate: {
            isEmail: true
        }
    },
    phone: {
        type: Sequelize.STRING
    }

}, {
    freezeTableName: true
});

// I have to create a custom validation. I don't know why the property "validate" doesn't work
Client.hook('beforeValidate', function(client) {
    if (!client.name || typeof client.name !== 'string') {
        return Sequelize.Promise.reject('Validation Error: invalid name');
    }
    return Sequelize.Promise.resolve(client);
});



module.exports = Client;