const Sequelize = require('sequelize');
const database = require(__base + 'database');
const Client = require('./client.model');
const Provider = require('./provider.model');

/** Relationship **/

const ClientProviders = database.define('client_providers', {
    clientId: {
        type: Sequelize.INTEGER,
        unique: 'client_providers'
    },
    providerId: {
        type: Sequelize.INTEGER,
        unique: 'client_providers',
        references: null
    }
});

Client.belongsToMany(Provider, {
    through: ClientProviders,
    foreignKey: 'clientId'
});

module.exports = ClientProviders;