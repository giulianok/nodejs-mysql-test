const Sequelize = require('sequelize');
const database = require(__base + 'database');

const Provider = database.define('provider', {

    name: {
        type: Sequelize.STRING,
        validate: {
            notEmpty: true,
            min: 3,
            max: 20
        }
    }

}, {
    freezeTableName: true
});

Provider.hook('beforeValidate', function(provider) {
    if (!provider.name || typeof provider.name !== 'string') {
        return Sequelize.Promise.reject('Validation Error: invalid name');
    }
    return Sequelize.Promise.resolve(provider);
});


module.exports = Provider;