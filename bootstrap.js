const Client = require('./api/models/client.model');
const Provider = require('./api/models/provider.model');
const ClientProviders = require('./api/models/clientProviders.model');


Client.sync({force: __recreate_tables})
    .then(function () {

        Provider.sync({force: __recreate_tables})
            .then(function () {

                ClientProviders.sync({force: __recreate_tables})
                    .then(function () {

                    });

            });

    });



