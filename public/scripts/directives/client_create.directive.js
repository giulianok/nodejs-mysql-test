angular.module('InterviewTest')
    .directive('clientCreate', function (ClientService, ProviderService, $rootScope) {
        return {
            templateUrl: '/templates/clients_create.html',
            link: function ($scope) {

                $scope.editing = null;
                $scope.selectedProviders = [];

                $scope.$on('editing-client', function (event, client) {
                    $scope.editing = client.id;
                    $scope.name = client.name || '';
                    $scope.email = client.email || '';
                    $scope.phone = client.phone || '';

                    if (client.providers && client.providers.length) {
                        client.providers.forEach((provider) => {
                            $scope.selectedProviders.push(provider.id);
                        });
                    }

                });

                $scope.onSubmit = function () {
                    const data = {
                        name: $scope.name,
                        email: $scope.email || null,
                        phone: $scope.phone || null,
                    };

                    if ($scope.selectedProviders && $scope.selectedProviders.length) {
                        data.providers = $scope.selectedProviders;
                    }

                    if ($scope.editing) {
                        console.log('editing');
                    } else {
                        console.log('not editing');
                    }

                    const action = ($scope.editing)
                        ? ClientService.update($scope.editing, data)
                        : ClientService.create(data)
                    ;

                    action
                        .then((response) => {
                            $scope.$emit('refresh-clients');
                            $('#createClientModal').modal('hide');
                        })
                        .catch((err) => {
                            console.error(err);
                        })
                    ;
                };

                $scope.onAddProvider = function () {

                    const name = $scope.new_provider;

                    if (!name) {
                        return alert('Please enter the provider name');
                    }

                    const data = {
                        name: name
                    };

                    ProviderService.create(data)
                        .then((response) => {
                            $scope.$emit('refresh-providers');
                            $scope.new_provider = '';
                        })
                        .catch((err) => {
                            console.error(err);
                        })
                    ;
                };

                $scope.selectProvider = function (id) {

                    var idx = $scope.selectedProviders.indexOf(id);

                    // is currently selected
                    if (idx > -1) {
                        $scope.selectedProviders.splice(idx, 1);
                    }

                    // is newly selected
                    else {
                        $scope.selectedProviders.push(id);
                    }

                };

                $scope.clear = function () {
                    $scope.editing = null;
                    $scope.name = '';
                    $scope.email = '';
                    $scope.phone = '';
                    $scope.selectedProviders = [];
                };

                $scope.isProviderSelected = function (id) {
                    return ($scope.selectedProviders.indexOf(id) > -1);
                };

                $scope.onEditProvider = function (id) {
                    $scope.editing_provider = id;
                    $scope.new_provider_name = $scope.providers.find(obj => obj.id == id).name;
                };

                $scope.editProvider = function (keyEvent) {
                    if (keyEvent.which !== 13)
                        return;

                    const id = $scope.editing_provider;

                    if (id) {

                        if (!$scope.new_provider_name) {
                            return alert(`Please enter a Provider name`);
                        }

                        const data = {
                            name: $scope.new_provider_name
                        };

                        ProviderService.update(id, data)
                            .then(() => {
                                $scope.$emit('refresh-providers');
                                $scope.$emit('refresh-clients');
                                $scope.editing_provider = null;
                            })
                        ;

                    }
                };

                $scope.onDelete = function (id) {
                    if (!id) {
                        return;
                    }

                    ProviderService.delete(id)
                        .then(() => {
                            $scope.$emit('refresh-providers');
                            $scope.$emit('refresh-clients');
                        })
                        .catch((err) => {
                            console.error(err);
                        })
                };


                $('#createClientModal').on('hidden.bs.modal', function () {
                    $scope.clear();
                    $scope.$apply();
                });


            }
        }
    });