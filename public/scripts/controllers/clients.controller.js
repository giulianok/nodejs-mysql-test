angular.module('InterviewTest')
.controller('clients.controller', function (ClientService, ProviderService, $scope, $rootScope) {


    getClients();

    $scope.$on('refresh-clients', function () {
        console.log('refreshing clients');
        getClients();
    });


    getProviders();

    $scope.$on('refresh-providers', function () {
        console.log('refreshing providers');
        getProviders();
    });



    $scope.onRemove = function (id) {
        if (!id) {
            throw new Error(`ID is required`);
        }

        ClientService.delete(id)
            .then(() => {
                getClients();
            })
            .catch((err) => {
                console.error(err);
            })
        ;
    };

    $scope.onEdit = function (client) {
        $scope.$emit('editing-client', client);
    };



    function getClients() {
        ClientService.getAll()
            .then((result) => {
                $scope.clients = result.data;
            });
    }

    function getProviders() {
        console.log('getProviders');
        ProviderService.getAll()
            .then((result) => {
                $scope.providers = result.data;
            });
    }

});