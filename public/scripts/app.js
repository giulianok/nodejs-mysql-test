'use strict';

const $app = angular.module('InterviewTest', [
    'ngRoute'
]);


$app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/templates/clients.html',
            controller: 'clients.controller'
        })
        .otherwise({
            redirectTo: '/'
        });

});

