angular.module('InterviewTest')
    .factory('ClientService', function ($http) {

        function Service() {
            this.headers = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
        }

        Service.prototype.getAll = function () {
            return $http.get('/clients');
        };

        Service.prototype.create = function (data) {
            const req = Object.assign(this.headers, {
                method: 'PUT',
                url: '/client_create',
                data: data
            });
            return $http(req);
        };

        Service.prototype.delete = function (id) {
            const req = Object.assign(this.headers, {
                method: 'DELETE',
                url: `/client/${id}`
            });
            return $http(req);
        };

        Service.prototype.update = function (id, data) {
            const req = Object.assign(this.headers, {
                method: 'POST',
                url: `/client/${id}`,
                data: data
            });
            return $http(req);
        };

        return new Service();

    });