angular.module('InterviewTest')
    .factory('ProviderService', function ($http) {

        function Service() {
            this.headers = {
                headers: {
                    'Content-Type': 'application/json'
                }
            };
        }

        Service.prototype.getAll = function () {
            return $http.get('/providers');
        };

        Service.prototype.create = function (data) {
            const req = Object.assign(this.headers, {
                method: 'PUT',
                url: '/provider_create',
                data: data
            });
            return $http(req);
        };

        Service.prototype.delete = function (id) {
            const req = Object.assign(this.headers, {
                method: 'DELETE',
                url: `/provider/${id}`
            });
            return $http(req);
        };

        Service.prototype.update = function (id, data) {
            const req = Object.assign(this.headers, {
                method: 'POST',
                url: `/provider/${id}`,
                data: data
            });
            return $http(req);
        };

        return new Service();

    });