const config = {

    database: {
        host: 'localhost',
        db: 'interview_test',
        user: 'root',
        pass: 'root',
        dialect: 'mysql'
    }

};

module.exports = config;