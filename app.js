'use strict';

global.__base = __dirname + '/';
global.__recreate_tables = true;

var SwaggerExpress = require('swagger-express-mw');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));

app.set('views', './views');
app.set('view engine', 'pug');


app.get('/', function (req, res) {
  res.render('index');
});


require('./bootstrap');


SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  setTimeout(function () {
    console.log('\n\nOpen this url in your Browser:\nhttp://127.0.0.1:' + port + '\n\n');
  }, 500);
});
