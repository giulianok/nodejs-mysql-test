# Practical Test

This is the practical test sent on December 2.
The following languages, frameworks and tools have been used base on the requirements:
HTML, CSS with Bootstrap, Javascript, AngularJS 1.x, NodeJS, MySQL, Sequelize (MySQL ORM), Swagger

### Instructions
Instructions to setup and run the app.
Make sure you have installed:
- NodeJS: v6.3.1
- NPM: 3.10.3
- MySQL: 14.14

#### Database
Open the following file

```sh
/config.js
```

Change the 'database' information if it's necessary

#### Run the app
Run the following command to install the dependencies and run the app. Make sure that your database is setup correctly and running

```sh
$ npm install
$ npm start
```


**Thanks**
