const Sequelize = require('sequelize');
const Config = require('./config');

const sequelize = new Sequelize(Config.database.db, Config.database.user, Config.database.pass, {
    host: Config.database.host,
    dialect: Config.database.dialect,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});

module.exports = sequelize;
